<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Demande des prêts</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Motant</th>
                                        <th>Date de demande</th>
                                        <th>Ajouté(e) par</th>
                                        <th>Valider</th>
                                        <th>Rejecter</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($listePrets as $pret):?>
                                        <tr>
                                            <td><?php echo $pret->nom_encadreur;?></td>
                                            <td><?php echo $pret->prenom_encadreur;?></td>
                                            <td><?php echo $pret->montant_pret;?>fdj</td>
                                            <td><?php echo $pret->date_pret;?></td>
                                            <td>
                                                <?php echo $pret->login;?>
                                            </td>
                                            <td><a href="<?php echo base_url();?>Accueil/validerPret/<?php echo $pret->id_pret;?>"><i class="fa fa-check" aria-hidden="true"></i></a></td>
                                            <td><a href="<?php echo base_url();?>Accueil/rejectPret/<?php echo $pret->id_pret;?>"><i class="fa fa-window-close " aria-hidden="true" style="color: red;"></i></a></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>