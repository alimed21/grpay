<!-- BREADCRUMB-->
<section class="au-breadcrumb m-t-75">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="au-breadcrumb-content">
                        <div class="au-breadcrumb-left">
                            <span class="au-breadcrumb-span">Bienvenu à la page d'accueil</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END BREADCRUMB-->
<!-- STATISTIC-->
<section class="statistic">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number"><?php echo $elevesInscrits->nbre;?></h2>
                        <span class="desc">Elèves inscrits</span>
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number"><?php echo $encadreurs->encadreur;?></h2>
                        <span class="desc">Encadreurs</span>
                        <div class="icon">
                            <i class="zmdi zmdi-account-o"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number"><?php $depensesTotal = $depenses->depense + $depensesSalaire->depenseSalaire + $depensesAutre->depenseAutre; echo $depensesTotal;?></h2>
                        <span class="desc">Dépenses total</span>
                        <div class="icon">
                            <i class="zmdi zmdi-money"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="statistic__item">
                        <h2 class="number"><?php echo $recettesTotal->montant?></h2>
                        <span class="desc">Recette total</span>
                        <div class="icon">
                            <i class="zmdi zmdi-money"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END STATISTIC-->
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8">
                    <!-- RECENT REPORT 2-->
                    <div class="recent-report2">
                        <h3 class="title-3">Demande des prêts</h3>
                        <div class="chart-info">
                            <div class="col-lg-12">
                                <div class="table-responsive table--no-card m-b-30">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nom</th>
                                            <th>Prénom</th>
                                            <th>Montant</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php if($derniersPrets != NULL):?>
                                                <?php foreach ($derniersPrets as $pret):?>
                                                    <tr>
                                                        <td><?php echo $pret->id_pret;?></td>
                                                        <td><?php echo $pret->nom_encadreur;?></td>
                                                        <td><?php echo $pret->prenom_encadreur;?></td>
                                                        <td><?php echo $pret->montant_pret;?>fdj</td>
                                                        <td><?php echo $pret->date_pret;?></td>
                                                        <td>
                                                            <a href="<?php echo base_url();?>Accueil/validerPret/<?php echo $pret->id_pret;?>"><i class="fa fa-check" aria-hidden="true"></i></a>
                                                            |
                                                            <a href="<?php echo base_url();?>Accueil/rejectPret/<?php echo $pret->id_pret;?>"><i class="fa fa-window-close" aria-hidden="true" style="color: red;"></i></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            <?php else:?>
                                                Pas de demander
                                            <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END RECENT REPORT 2             -->
                </div>
                <div class="col-xl-4">
                    <!-- TASK PROGRESS-->
                    <div class="task-progress">
                        <h3 class="title-3">Recettes</h3>
                        <div class="au-skill-container">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Date</th>
                                    <th>Montant</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($lastRecettes != NULL):?>
                                    <?php foreach ($lastRecettes as $recette):?>
                                        <tr>
                                            <td><?php echo $recette->id_paie;?></td>
                                            <td><?php echo $recette->date_paie;?></td>
                                            <td><?php echo $recette->montant_paie;?></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else:?>
                                    Pas de demander
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END TASK PROGRESS-->
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6">
                    <!-- USER DATA-->
                    <div class="user-data m-b-40">
                        <h3 class="title-3 m-b-30">
                            <i class="zmdi zmdi-account-calendar"></i>Liste des utilisateurs</h3>
                        <div class="table-responsive table-data" style="height: 346px;">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>N°</td>
                                    <td>Login</td>
                                    <td>Email</td>
                                    <td>Rôle</td>
                                </tr>
                                </thead>
                                <tbody>
                                 <?php if($UsersFiveLast != NULL):?>
                                    <?php foreach ($UsersFiveLast as $u):?>
                                        <tr>
                                            <td><?php echo $u->id_user;?></td>
                                            <td><?php echo $u->login;?></td>
                                            <td><?php echo $u->email;?></td>
                                            <td>
                                                <?php if($u->type_user == "admin"):?>
                                                    <span class="role admin">admin</span>
                                                <?php else:?>
                                                    <span class="role user">caissier</span>
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                 <?php else:?>

                                 <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END USER DATA-->
                </div>
                <div class="col-xl-6">
                    <!-- MAP DATA-->
                    <div class="map-data m-b-40">
                        <h3 class="title-3 m-b-30">
                            <i class="zmdi zmdi-map"></i>Historiques</h3>

                        <div class="table-wrap">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td>N°</td>
                                    <td>Login</td>
                                    <td>Action</td>
                                    <td>Date</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if($usersHistory != NULL):?>
                                    <?php foreach($usersHistory as $his):?>
                                        <tr style="">
                                            <td><?php echo $his->id_his;?></td>
                                            <td><?php echo $his->login;?></td>
                                            <td><?php echo $his->action_his;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($his->date_his));?></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END MAP DATA-->
                </div>
            </div>
        </div>
    </div>
</section>