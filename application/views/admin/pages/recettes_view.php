<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Recettes</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>N°facture</th>
                                        <th>N°adhérent</th>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Montant payer</th>
                                        <th>Date de paiement</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($listePayaments as $se):?>
                                        <tr>
                                            <td><?php echo $se->id_paie;?></td>
                                            <td><?php echo $se->id_eleve;?></td>
                                            <td><?php echo $se->nom_eleve;?></td>
                                            <td><?php echo $se->prenom_eleve;?></td>
                                            <td><?php echo $se->montant_paie;?> fdj</td>
                                            <td><?php echo date("d-m-Y", strtotime($se->date_paie));?></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
