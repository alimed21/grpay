<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des prêts</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Motant</th>
                                        <th>Date de demande</th>
                                        <th>Etat</th>
                                        <th>Rembrouser</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($listePretEncadreur as $pret):?>
                                        <tr>
                                            <td><?php echo $pret->nom_encadreur;?></td>
                                            <td><?php echo $pret->prenom_encadreur;?></td>
                                            <td><?php echo $pret->montant_pret;?>fdj</td>
                                            <td><?php echo $pret->date_pret;?></td>
                                            <td>
                                                <?php if($pret->pret_etat == NULL):?>
                                                    En cour
                                                <?php elseif($pret->pret_etat == 1):?>
                                                    Prêt accordé
                                                <?php else:?>
                                                    Prêt non-accord
                                                <?php endif;?>
                                            </td>
                                            <?php if($pret->pret_rembourser == 1):?>
                                                <td>
                                                   Prêt rembrouser
                                                </td>
                                            <?php else:?>
                                                <td>
                                                    Prêt non rembourser
                                                </td>
                                            <?php endif;?>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>