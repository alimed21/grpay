<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des paiements des encadreurs</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Motant</th>
                                        <th>Date de paiement</th>
                                        <th>Payer par</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($paieEncadreur as $paie):?>
                                        <tr>
                                            <td><?php echo $paie->nom_encadreur;?></td>
                                            <td><?php echo $paie->prenom_encadreur;?></td>
                                            <td><?php echo $paie->montant_paie_encad;?>fdj</td>
                                            <td><?php echo date("d-m-Y", strtotime($paie->date_add_paie_encad));?></td>
                                            <td>
                                                <?php echo $paie->login;?>
                                            </td>
                                           <!-- <td><a href="<?php echo base_url();?>Caissier/inscriptionSupprimer/<?php echo $paie->id_paie_encad;?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>-->
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>