<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des impayés</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <!--Button initialiser-->
                        <div class="col-md-12" style="margin-bottom: 11px;">
                            <a href="<?php echo base_url();?>Caissier/initialiser">
                                <button class="btn btn-danger">
                                    Initialiser
                                </button>
                            </a>
                        </div>
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Adresse</th>
                                        <th>Nationalité</th>
                                        <th>Date de naissance</th>
                                        <th>Paiement</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($listesPaie as $user):?>
                                        <tr>
                                            <td><?php echo $user->nom_eleve;?></td>
                                            <td><?php echo $user->prenom_eleve;?></td>
                                            <td><?php echo $user->adresse_eleve;?></td>
                                            <td><?php echo $user->nationalite_eleve;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($user->date_naissance));?></td>
                                            <td><button class="btn btn-primary" data-toggle="modal" data-target="#payer<?php echo $user->id_eleve;?>">Payer</button></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal paiement -->
<?php foreach ($eleves as $el):?>
    <div class="modal fade" id="payer<?php echo $el->id_eleve;?>" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Effectuer un paiement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?php echo base_url();?>Caissier/Paiement" method="post">
                        <input type="hidden" name="id_eleve" value="<?php echo $el->id_eleve;?>">
                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Montant payer</label>
                            <input id="paie" name="paie" type="text" class="form-control" value="10000" required>
                        </div>
                        <div class="form-group has-success">
                            <label for="cc-name" class="control-label mb-1">Date de paiement</label>
                            <input id="datePaie" name="datePaie" type="date" class="form-control" required>
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>


                        <div>
                            <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                <i class="fa fa-lock fa-lg"></i>&nbsp;
                                <span id="payment-button-amount">Payer<span>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>
<!-- end modal medium -->
