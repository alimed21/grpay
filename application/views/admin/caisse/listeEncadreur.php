<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des encadreurs</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Nationalité</th>
                                        <th>Fonction</th>
                                        <th>Pièces joints</th>
                                        <th>Ajouté(e) par</th>
                                        <th>Ajouté(e) le</th>
                                        <th>Prêt</th>
                                        <th>Présent</th>
                                        <th>Absent</th>
                                        <th>Supprimer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($encadreurs as $encadre):?>
                                        <tr>
                                            <td><?php echo $encadre->nom_encadreur;?></td>
                                            <td><?php echo $encadre->prenom_encadreur;?></td>
                                            <td><?php echo $encadre->nationalite_encadreur;?></td>
                                            <td><?php echo $encadre->fonction;?></td>
                                            <td>
                                                <?php if($encadre->piecesjoint != NULL):?>
                                                    <a href="<?php echo base_url();?><?php echo $encadre->piecesjoint;?>" download><button class="btn btn-primary">Télécharger</button></a>
                                                <?php else:?>
                                                    Pas de pièce
                                                <?php endif;?>
                                            </td>
                                            <td><?php echo $encadre->login;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($encadre->date_add));?></td>
                                            <td><button class="btn btn-primary" data-toggle="modal" data-target="#payer<?php echo $encadre->id_encadreur;?>">Prêt</button></td>
                                            <td>
                                                <a href="<?php echo base_url();?>Caissier/presenceEncadreur/<?php echo $encadre->id_encadreur;?>">
                                                    <button class="btn btn-success">Présent</button>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url();?>Caissier/absenceEncadreur/<?php echo $encadre->id_encadreur;?>">
                                                    <button class="btn btn-danger">Absent</button>
                                                </a>
                                            </td>
                                            <td><a href="<?php echo base_url();?>Caissier/encadreurSupprimer/<?php echo $encadre->id_encadreur;?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal paiement -->
<?php foreach ($encadreurs as $encadre):?>
    <div class="modal fade" id="payer<?php echo $encadre->id_encadreur;?>" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Demande d'un prêt</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?php echo base_url();?>Caissier/Pret" method="post">
                        <input type="hidden" name="id_encadreur" value="<?php echo $encadre->id_encadreur;?>">
                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Montant du prêt</label>
                            <input id="pret" name="pret" type="text" class="form-control" value="100.00" required>
                        </div>
                        <div class="form-group has-success">
                            <label for="cc-name" class="control-label mb-1">Date de paiement</label>
                            <input id="datePret" name="datePret" type="date" class="form-control" required>
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>


                        <div>
                            <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                <i class="fa fa-lock fa-lg"></i>&nbsp;
                                <span id="payment-button-amount">Payer<span>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>
<!-- end modal medium -->