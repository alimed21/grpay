<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Modifier une inscription</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <?php if (isset($error_message)) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error_message; ?>
                            </div>
                        <?php } ?>
                        <div class="card-body card-block">
                            <?php echo form_open_multipart('Caissier/modificationInscription');?>
                                <?php foreach ($elevesDetail as $el):?>
                                    <div class="card-title">
                                        <h3 class="text-center title-2">Information de l'enfant</h3>
                                    </div>
                                    <input type="hidden" name="id_eleve" value="<?php echo $el->id_eleve;?>">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" id="nom" name="nom" placeholder="Nom..." value="<?php echo $el->nom_eleve;?>" class="form-control" >
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('nom'); ?></span>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" id="prenom" name="prenom" placeholder="Prénom..." value="<?php echo $el->prenom_eleve;?>" class="form-control" >
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('prenom'); ?></span>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-address-card"></i>
                                            </div>
                                            <input type="text" id="adresse" name="adresse" placeholder="Adresse..." value="<?php echo $el->adresse_eleve;?>" class="form-control" >
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('adresse'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-flag"></i>
                                            </div>
                                            <input type="text" id="nationalite" name="nationalite" placeholder="Nationalité..." value="<?php echo $el->nationalite_eleve;?>" class="form-control" >
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('nationalite'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="date" id="date_naissance" name="date_naissance" placeholder="Date de naissance..." value="<?php echo $el->date_naissance;?>" class="form-control" >
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('date_naissance'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" id="telephone" name="telephone" placeholder="Téléphone..." value="<?php echo $el->telephone;?>" class="form-control" >
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('telephone'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-cc-diners-club"></i>
                                            </div>
                                            <input type="text" id="ancien_club" name="ancien_club" placeholder="Ancien club..." value="<?php echo $el->ancien_club;?>" class="form-control">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('ancien_club'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <select name="type" id="type" class="form-control">
                                                <?php if($el->catégorie == "u11"):?>
                                                    <option value="u11" selected>U11</option>
                                                    <option value="u13">U13</option>
                                                    <option value="u15">U15</option>
                                                <?php elseif ($el->catégorie == "u13"):?>
                                                    <option value="u11">U11</option>
                                                    <option value="u13" selected>U13</option>
                                                    <option value="u15">U15</option>
                                                <?php else:?>
                                                    <option value="u11">U11</option>
                                                    <option value="u13">U13</option>
                                                    <option value="u15" selected>U15</option>
                                                <?php endif;?>

                                            </select>
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('type'); ?></span>
                                    </div>

                                    <div class="card-title">
                                        <h3 class="text-center title-2">Information tuteur</h3>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" id="nom_prenom" name="nom_prenom" placeholder="Nom et Prénom..." class="form-control" value="<?php echo $el->nom_prenom_pere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('nom_prenom'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-address-card"></i>
                                            </div>
                                            <input type="text" id="adresse_pere" name="adresse_pere" placeholder="Adresse..." class="form-control" value="<?php echo $el->adresse_pere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('adresse_pere'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-mobile-phone"></i>
                                            </div>
                                            <input type="text" id="phone_pere" name="phone_pere" placeholder="Téléphone portable..." class="form-control" value="<?php echo $el->tele_pere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('phone_pere'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" id="phoneFix_pere" name="phoneFix_pere" placeholder="Téléphone domicile..." class="form-control" value="<?php echo $el->fix_pere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('phoneFix_pere'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input type="email" id="email_pere" name="email_pere" placeholder="Mails..." class="form-control" value="<?php echo $el->mail_pere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('email_pere'); ?></span>
                                    </div>

                                    <div class="card-title">
                                        <h3 class="text-center title-2">Et</h3>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" id="nom_prenom_mere" name="nom_prenom_mere" placeholder="Nom et Prénom..." class="form-control" value="<?php echo $el->nom_prenom_mere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('nom_prenom_mere'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-address-card"></i>
                                            </div>
                                            <input type="text" id="adresse_mere" name="adresse_mere" placeholder="Adresse..." class="form-control" value="<?php echo $el->adresse_mere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('adresse_mere'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-mobile-phone"></i>
                                            </div>
                                            <input type="text" id="phone_mere" name="phone_mere" placeholder="Téléphone portable..." class="form-control" value="<?php echo $el->tele_mere;?>">
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('phone_mere'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="">
                                                L'enfant est-il autorisé à sortir seul?
                                            </div>
                                            <br>
                                            <?php if($el->autorisation == "oui"):?>
                                                <input type="radio" id="autorisation" name="autorisation" value="oui" class="form-check-label" checked> Oui
                                                <input type="radio" id="autorisation2" name="autorisation" value="non" class="form-check-label"> Non
                                            <?php else:?>
                                                <input type="radio" id="autorisation" name="autorisation" value="oui" class="form-check-label"> Oui
                                                <input type="radio" id="autorisation2" name="autorisation" value="non" class="form-check-label" checked> Non
                                            <?php endif;?>
                                        </div>
                                        <span class="infoMessage"><?php echo form_error('autorisation'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <input type="file" id="userfile" name="userfile" class="form-control" >
                                    </div>

                                    <div class="form-actions form-group">
                                        <button type="submit" class="btn btn-success btn-sm">Ajouter</button>
                                    </div>
                                <?php endforeach;?>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>