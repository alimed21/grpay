<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);

        body {

            font-family: "Roboto", helvetica, arial, sans-serif;
            font-size: 16px;
            font-weight: 400;
            text-rendering: optimizeLegibility;
        }

        div.table-title {
            display: block;
            margin: auto;
            max-width: 600px;
            padding:5px;
            width: 100%;
        }

        .table-title h3 {
            color: #fafafa;
            font-size: 30px;
            font-weight: 400;
            font-style:normal;
            font-family: "Roboto", helvetica, arial, sans-serif;
            text-transform:uppercase;
        }


        /*** Table Styles **/

        .table-fill, .table-fill2, .table-fill3  {
            background: white;
            border-radius:3px;
            border-collapse: collapse;
            margin: auto;
            padding:5px;
            width: 100%;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
            animation: float 5s infinite;
        }


        th {
            color:white;;
            background:#1b1e24;
            border-bottom:4px solid #9ea7af;
            border-right: 1px solid #343a45;
            font-size:23px;
            font-weight: 100;
            padding:5px;
            text-align:left;
            vertical-align:middle;
        }

        th:first-child {
            border-top-left-radius:3px;
        }

        th:last-child {
            border-top-right-radius:3px;
            border-right:none;
        }

        tr {
            border-top: 1px solid #C1C3D1;
            border-bottom: 1px solid #C1C3D1;
            color:#666B85;
            font-size:16px;
            font-weight:normal;
        }

        tr:hover td {
            background:#4E5066;
            color:#FFFFFF;
            border-top: 1px solid #22262e;
        }

        tr:first-child {
            border-top:none;
        }

        tr:last-child {
            border-bottom:none;
        }

        tr:nth-child(odd) td {
            background:#EBEBEB;
        }

        tr:nth-child(odd):hover td {
            background:#4E5066;
        }

        tr:last-child td:first-child {
            border-bottom-left-radius:3px;
        }

        tr:last-child td:last-child {
            border-bottom-right-radius:3px;
        }

        td {
            background:#FFFFFF;
            padding:5px;
            text-align:left;
            vertical-align:middle;
            font-weight:300;
            font-size:15px;
            border-right: 1px solid #C1C3D1;
            height: 30px;
        }

        td:last-child {
            border-right: 0px;
        }

        th.text-left {
            text-align: left;
        }

        th.text-center {
            text-align: center;
        }

        th.text-right {
            text-align: right;
        }

        td.text-left {
            text-align: left;
        }

        td.text-center {
            text-align: center;
        }

        td.text-right {
            text-align: right;
        }
        caption {
            background: #6AB2E7;
            padding: 10px;
            font-size:20px;
            color: white;
            text-align: left;
        }
        td{
            color:black;
        }
        .image{
            text-align:right;
        }
        .title_tr td{
            font-size: 20px;
            color: black;
            background-color: #1b1e24 !important;
            width: 45%;
            height: 6%;
        }
        .corps{
            margin-top: -20px;
        }
    </style>
</head>
<body>
<div style="text-align:center"><img src="<?php echo base_url();?>assets/admin/images/banniere.png" alt="Logo" style="width: 100%;"></div>
<h1 style="text-align:center">Fiche d'inscription</h1>
<div class="col-md-12">
    <?php foreach($form as $row):?>
        <img src="<?php echo base_url();?><?php echo $row->image_eleve;?>" style=" width: 167px;height: auto;">
        <p style="margin-bottom: 25px;">Adhérent n°<?php echo $row->id_eleve;?></p>
    <?php endforeach;?>
</div>
<div class="corps">
    <table class="table-fill" style="margin-top: 10px;">
        <?php foreach($form as $row):?>
            <caption>Information de l'enfant</caption>

            <tr>
                <td style="width: 332px;">Nom et prénom </td>
                <td><?php echo $row->nom_eleve; ?> <?php echo $row->prenom_eleve; ?></td>
            </tr>
            <tr>
                <td style="width: 332px;">Adresse </td>
                <td><?php echo $row->adresse_eleve; ?></td>
            </tr>
            <tr>
                <td>Date de naissance </td>
                <td>le <?php echo date('d-m-Y', strtotime($row->date_naissance)); ?></td>
            </tr>
            <tr>
                <td>Nationalité</td>
                <td><?php echo $row->nationalite_eleve; ?></td>
            </tr>
            <tr>
                <td>Téléphne</td>
                <td><?php echo $row->telephone; ?></td>
            </tr>
            <tr>
                <td>Ancien club</td>
                <td><?php echo $row->ancien_club; ?></td>
            </tr>
            <tr>
                <td>Catégorie</td>
                <td><?php echo $row->catégorie	; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
    <table class="table-fill">
        <?php foreach($form as $row):?>
            <caption>Information du père</caption>
            <tr>
                <td style="width: 332px;">Nom et prénom du père</td>
                <td><?php echo $row->nom_prenom_pere; ?></td>
            </tr>
            <tr>
                <td>Adresse </td>
                <td><?php echo $row->adresse_pere; ?></td>
            </tr>
            <tr>
                <td>Téléphone portable</td>
                <td><?php echo $row->tele_pere; ?></td>
            </tr>
            <tr>
                <td>Téléphone domicile </td>
                <td><?php echo $row->fix_pere; ?></td>
            </tr>
            <tr>
                <td>Mail </td>
                <td><?php echo $row->mail_pere; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
    <br>
    <table class="table-fill">
        <caption>Informations de la mère</caption>
        <?php foreach($form as $row):?>
            <tr>
                <td style="width: 332px;">Nom et prénom de la mère</td>
                <td><?php echo $row->nom_prenom_mere; ?></td>
            </tr>
            <tr>
                <td>Adresse </td>
                <td><?php echo $row->adresse_mere; ?></td>
            </tr>
            <tr>
                <td>Téléphone portable</td>
                <td><?php echo $row->tele_mere; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
    <?php foreach ($form as $row):?>
        <p>L'enfant est-il autorisé à sortir seul ? <?php echo $row->autorisation;?></p>
    <?php endforeach;?>
</div>
</body>
</html>