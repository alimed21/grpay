<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Ajout d'un paiement</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Caissier/PaieEncadreur" method="post" class="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <select name="encadreur" id="encadreur" class="form-control">
                                            <option value="" disabled selected>Choisissez un encadreur</option>
                                            <?php foreach ($encadreurs as $encad):?>
                                                <option value="<?php echo $encad->id_encadreur;?>"><?php echo $encad->nom_encadreur;?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('username'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-money-bill-alt"></i>
                                        </div>
                                        <input type="number" id="paie" name="paie" placeholder="Montant à payer..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('paie'); ?></span>
                                </div>

                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-success btn-sm">Payer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>