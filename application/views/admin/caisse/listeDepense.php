<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des dépenses</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Montant</th>
                                        <th>Motif</th>
                                        <th>Date dépense</th>
                                        <th>Ajouté(e) par</th>
                                        <th>Ajouté(e) le</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($depenses as $dep):?>
                                        <tr>
                                            <td><?php echo $dep->montant_dep;?></td>
                                            <td><?php echo $dep->motif_dep;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($dep->date_dep));?></td>
                                            <td><?php echo $dep->login;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($dep->date_add));?></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
