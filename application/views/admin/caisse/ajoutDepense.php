<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Ajout d'une dépense</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Caissier/depenseForm" method="post" class="">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-file-text"></i>
                                        </div>
                                        <input type="text" id="titre" name="titre" placeholder="Motif du dépense..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('titre'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-money-bill-alt"></i>
                                        </div>
                                        <input type="number" id="paie" name="paie" placeholder="Montant à payer..." class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('paie'); ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="date" id="datePaie" name="datePaie" class="form-control">
                                    </div>
                                    <span class="infoMessage"><?php echo form_error('datePaie'); ?></span>
                                </div>

                                <div class="form-actions form-group">
                                    <button type="submit" class="btn btn-success btn-sm">Payer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>