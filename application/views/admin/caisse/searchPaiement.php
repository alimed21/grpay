<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Rechercher un paiement</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <div class="card-body card-block">
                            <form action="<?php echo base_url();?>Caissier/resultatRecherche" method="post" class="">
                                <div class="row form-group">
                                    <div class="col col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="search_text" name="search" placeholder="Recherche par n°adhérent" class="form-control">
                                            <br>
                                            <span class="infoMessage"><?php echo form_error('search'); ?></span>
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary">
                                                    <i class="fa fa-search"></i> Rechercher
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="result"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>