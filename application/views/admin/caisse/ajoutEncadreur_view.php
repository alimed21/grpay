<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">Fiche d'inscription</div>
                        <?php if ( $this->session->flashdata( 'error' ) ) :?>
                            <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                        <?php endif;?>
                        <?php if (isset($error_message)) { ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo $error_message; ?>
                            </div>
                        <?php } ?>
                        <div class="card-body card-block">
                            <?php echo form_open_multipart('Caissier/encadreur');?>
                            <div class="card-title">
                                <h3 class="text-center title-2">Identité</h3>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" id="nom" name="nom" placeholder="Nom..." class="form-control" >
                                </div>
                                <span class="infoMessage"><?php echo form_error('nom'); ?></span>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" id="prenom" name="prenom" placeholder="Prénom..." class="form-control" >
                                </div>
                                <span class="infoMessage"><?php echo form_error('prenom'); ?></span>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-flag"></i>
                                    </div>
                                    <input type="text" id="nationalite" name="nationalite" placeholder="Nationalité..." class="form-control" >
                                </div>
                                <span class="infoMessage"><?php echo form_error('nationalite'); ?></span>
                            </div>


                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" id="fonction" name="fonction" placeholder="Fonction..." class="form-control" >
                                </div>
                                <span class="infoMessage"><?php echo form_error('fonction'); ?></span>
                            </div>

                            <div class="form-group">
                                <input type="file" id="userfile" name="userfile" class="form-control" >
                            </div>

                            <div class="form-actions form-group">
                                <button type="submit" class="btn btn-success btn-sm">Ajouter</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>