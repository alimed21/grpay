<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header">Liste des impayés</div>
                    <?php if ( $this->session->flashdata( 'error' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('error'); ?></h2>
                    <?php endif;?>
                    <?php if ( $this->session->flashdata( 'sucess' ) ) :?>
                        <h2 class="infoMessage"><?php echo $this->session->flashdata('sucess'); ?></h2>
                    <?php endif;?>
                    <div class="row m-t-30">
                        <div class="col-md-12">
                            <!-- DATA TABLE-->
                            <div class="table-responsive m-b-40">
                                <table class="table table-borderless table-data3">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Adresse</th>
                                        <th>Nationalité</th>
                                        <th>Date de naissance</th>
                                        <th>Téléphone</th>
                                        <th>Dernière paiement</th>
                                        <th>Détails</th>
                                        <th>Paiement</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($elevesNonPayer as $user):?>
                                        <tr>
                                            <td><?php echo $user->nom_eleve;?></td>
                                            <td><?php echo $user->prenom_eleve;?></td>
                                            <td><?php echo $user->adresse_eleve;?></td>
                                            <td><?php echo $user->nationalite_eleve;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($user->date_naissance));?></td>
                                            <td><?php echo $user->telephone;?></td>
                                            <td><?php echo date("d-m-Y", strtotime($user->date_paie));?></td>
                                            <td><button class="btn btn-primary" data-toggle="modal" data-target="#scrollmodal<?php echo $user->id_eleve;?>">Voir plus</button></td>
                                            <td><button class="btn btn-primary" data-toggle="modal" data-target="#payer<?php echo $user->id_eleve;?>">Payer</button></td>
                                        </tr>
                                    <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- END DATA TABLE-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal fiche d'inscription -->
<?php foreach ($elevesNonPayer as $el):?>
    <div class="modal fade" id="scrollmodal<?php echo $el->id_eleve;?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Fiche d'inscription</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="entete">
                        <h3>Republique de Djibouti</h3><br><h5>Unité-Egalité-Paix</h5>
                    </div>
                    <div class="bannier"></div>
                    <div class="info col-md-4">
                        <h3>Fiche d'inscription</h3>
                    </div>
                    <div class="col-md-12 imgPhoto">

                        <p>Adhérent n°<?php echo $el->id_eleve;?></p>
                        <img src="<?php echo base_url();?><?php echo $el->image_eleve;?>">

                    </div>
                    <div class="col-md-12 infoComplet">
                        <h5>a) ENFANT</h5>
                        <p>Nom : <?php echo $el->nom_eleve;?>  <spam class="spam">Adresse: <?php echo $el->adresse_eleve;?></spam></p>
                        <p>Prénom : <?php echo $el->prenom_eleve;?>  <spam class="spam">Nationalité: <?php echo $el->nationalite_eleve;?></spam></p>
                        <p>Date de naissance : <?php echo $el->date_naissance;?>  <spam class="spam">Téléphone: <?php echo $el->telephone;?></spam></p>
                        <p>Ancien club : <?php echo $el->ancien_club;?>  <spam class="spam">Catégorie: <?php echo $el->catégorie;?></spam></p>

                        <br>
                        <h5>b) PARENTS OU PERSONNES HABILITEES A RECUPERER L'ENFANT A LA FIN DES ENTRAINEMENTS</h5>
                        <p>Nom et prénom: <?php echo $el->nom_prenom_pere;?>  <spam class="spam">Adresse: <?php echo $el->adresse_pere;?></spam></p>
                        <p>Téléphone portable : <?php echo $el->tele_pere;?>  <spam class="spam">Téléphone domicile: <?php echo $el->fix_pere;?></spam></p>
                        <p>Mails : <?php echo $el->mail_pere;?></p>
                        <h5>ET</h5>
                        <p>Nom et prénom: <?php echo $el->nom_prenom_mere;?>  <spam class="spam">Adresse: <?php echo $el->adresse_mere;?></spam></p>
                        <p>Téléphone portable : <?php echo $el->tele_mere;?>  <spam class="spam">Mails : <?php echo $el->mail_mere;?></spam></p>

                        <p>L'enfant est-il autorisé à sortir seul ? <?php echo $el->autorisation;?></p>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>

<!-- modal paiement -->
<?php foreach ($elevesNonPayer as $el):?>
    <div class="modal fade" id="payer<?php echo $el->id_eleve;?>" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Effectuer un paiement</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?php echo base_url();?>Caissier/Paiement" method="post">
                        <input type="hidden" name="id_eleve" value="<?php echo $el->id_eleve;?>">
                        <div class="form-group">
                            <label for="cc-payment" class="control-label mb-1">Montant payer</label>
                            <input id="paie" name="paie" type="text" class="form-control" value="100.00" required>
                        </div>
                        <div class="form-group has-success">
                            <label for="cc-name" class="control-label mb-1">Date de paiement</label>
                            <input id="datePaie" name="datePaie" type="date" class="form-control" required>
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>


                        <div>
                            <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                <i class="fa fa-lock fa-lg"></i>&nbsp;
                                <span id="payment-button-amount">Payer<span>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                </div>
            </div>
        </div>
    </div>
<?php endforeach;?>
<!-- end modal medium -->
