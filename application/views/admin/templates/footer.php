<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright">
                    <p>Copyright © 2020 BFCGR. Développer par <a href="#">CCEI</a>.</p>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- modal paiement -->
<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Recherche facture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url();?>Caissier/resultatRecherche" method="post">
                    <div class="form-group">
                        <label for="cc-payment" class="control-label mb-1">N°Adhérent</label>
                        <input id="search" name="search" type="text" class="form-control" placeholder="Saissiez le numéro de l'adhérent" required>
                    </div>
                    <div>
                        <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                            <i class="fa fa-search"></i>&nbsp;
                            <span id="payment-button-amount">Rechercher<span>
                        </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal medium -->

<!-- END PAGE CONTAINER-->
</div>

</div>

<!-- Jquery JS-->
<script src="<?php echo base_url();?>assets/admin/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="<?php echo base_url();?>assets/admin/vendor/slick/slick.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/wow/wow.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/animsition/animsition.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/circle-progress/circle-progress.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/select2/select2.min.js">
</script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.sampledata.js"></script>
<script src="<?php echo base_url();?>assets/admin/vendor/vector-map/jquery.vmap.world.js"></script>

<!-- Main JS-->
<script src="<?php echo base_url();?>assets/admin/js/main.js"></script>

<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('#example2').DataTable();
    } );
</script>

</body>

</html>
<!-- end document-->