<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Barreh football clubs</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url();?>assets/admin/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url();?>assets/admin/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url();?>assets/admin/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assets/admin/vendor/vector-map/jqvmap.min.css" rel="stylesheet" media="all">


    <link href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css" rel="stylesheet" media="all">

    <style>
        select.form-control:not([size]):not([multiple]) {
            height: calc(2.25rem + 0px) !important;
        }
    </style>

    <!-- Main CSS-->
    <link href="<?php echo base_url();?>assets/admin/css/theme.css" rel="stylesheet" media="all">

    <!--eChart-->
    <script src="<?php echo base_url();?>assets/echarts/dist/echarts.min.js"></script>


    <!-- Style CSS-->
    <style>
        .infoMessage{
            color:red;
            font-size: 15px;
        }
    </style>

</head>

<body class="animsition">
<div class="page-wrapper">
    <!-- MENU SIDEBAR-->
    <aside class="menu-sidebar2">
        <div class="logo">
            <a href="#">
                <h3 style="color: white;font-size: 18px">Barreh football clubs</h3>
            </a>
        </div>
        <div class="menu-sidebar2__content js-scrollbar1">
            <div class="account2">
                <?php foreach ($infoUser as $user):?>
                    <div class="image img-cir img-120">
                        <img src="<?php echo base_url();?><?php echo $user->image_user;?>" alt="Photo" />
                    </div>
                    <h4 class="name"><?php echo $user->login;?></h4>
                    <a href="<?php echo base_url();?>Login/logout">Déconnexion</a>
                <?php endforeach;?>
            </div>
            <nav class="navbar-sidebar2">
                <ul class="list-unstyled navbar__list">
                    <?php if ($this->session->userdata('type_user') == "admin" ):?>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Accueil">
                                <i class="fas fa-tachometer-alt"></i>Accueil
                            </a>
                        </li>
                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>Utilisateur
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/ajoutUtilisateur">
                                        Ajouter un utilisateur
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/listesUtilisateurs">
                                        Liste des utilisateurs
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-credit-card" aria-hidden="true"></i>Paiement
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/listesPaiements">
                                        Liste des paiements
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/listesPaiements">
                                        Liste des impayés
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/listePrets">
                                        Demande des prêts
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-money-bill-alt" aria-hidden="true"></i>Dépenses
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/paiementEncadreurs">
                                        Salaire encadreurs
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/pretEncadreurs">
                                        Prêt
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Accueil/autreDepenses">
                                        Autres dépenses
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Accueil/recettes">
                                <i class="fas fa-money-bill-alt"></i>Recettes
                            </a>
                        </li>
                        <li class="active">
                            <a class="js-arrow" href="<?php echo base_url();?>Accueil/historiques">
                                <i class="fas fa-history"></i>Historiques
                            </a>
                        </li>
                    <?php else:?>
                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>Inscriptions
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier">
                                        Ajouter une inscription
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/listesEnfants">
                                        Liste des inscriptions
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/listePaiementEleve">
                                        Liste des impayés
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Encadreurs
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/ajoutEncadreur">
                                        Ajouter un encadreur
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/listesEncadreur">
                                        Liste des encadreurs
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/listePret">
                                        Liste des prêts
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/lisetePresence">
                                        Liste des présences
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-credit-card"></i>Paiemennt
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="active">
                                    <a class="js-arrow" href="<?php echo base_url();?>Caissier/paiementEncadreur">
                                      Ajouter un paiement
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/listesPaieEncadreur">
                                        Liste des paiements
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub active">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-money-bill-alt"></i>Dépense
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li class="active">
                                    <a class="js-arrow" href="<?php echo base_url();?>Caissier/depense">
                                        Ajouter une dépense
                                    </a>
                                </li>
                                <li class="active">
                                    <a href="<?php echo base_url();?>Caissier/listesDepense">
                                        Liste des dépenses
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="#" data-toggle="modal" data-target="#search">
                                 <i class="fa fa-search"></i>Recherche paiement
                            </a>
                        </li>
                    <?php endif;?>

                </ul>
            </nav>
        </div>
    </aside>
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container2">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop2">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap2">
                        <div class="logo d-block d-lg-none">
                            <a href="#">
                                <h3 style="color: white;font-size: 18px">Barreh football clubs</h3>
                            </a>
                        </div>
                        <div class="header-button2">

                            <div class="header-button-item mr-0 js-sidebar-btn">
                                <i class="zmdi zmdi-menu"></i>
                            </div>
                            <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="<?php echo base_url();?>Profile">
                                            <i class="zmdi zmdi-account"></i>Profile</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="<?php echo base_url();?>Parametres">
                                            <i class="zmdi zmdi-settings"></i>Parametres</a>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="<?php echo base_url();?>Login/logout">
                                            <i class="zmdi zmdi-sign-in"></i>Déconnexion</a>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
            <div class="logo">
                <a href="#">
                    <img src="<?php echo base_url();?>assets/admin/images/icon/logo-white.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar2">
                <div class="account2">
                    <?php foreach ($infoUser as $user):?>
                        <div class="image img-cir img-120">
                            <img src="<?php echo base_url();?><?php echo $user->image_user;?>" alt="Photo" />
                        </div>
                        <h4 class="name"><?php echo $user->username;?></h4>
                    <?php endforeach;?>
                    <a href="<?php echo base_url();?>Admin/Login/logout">Déconnexion</a>
                </div>
                <nav class="navbar-sidebar2">
                    <ul class="list-unstyled navbar__list">
                        <?php if ($this->session->userdata('nom_type') == "Secretariat" ):?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretariat">
                                    <i class="fas fa-tachometer-alt"></i>Liste des plaintes
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretariat/Affecter">
                                    <i class="fas fa-tachometer-alt"></i>Affectation des plaintes
                                </a>
                            </li>
                        <?php elseif($this->session->userdata('nom_type') == "Agent"):?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Agent">
                                    <i class="fas fa-tachometer-alt"></i>Voir les plaintes
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Agent/Affecter">
                                    <i class="fas fa-tachometer-alt"></i>Affecter les plaintes
                                </a>
                            </li>
                        <?php elseif($this->session->userdata('nom_type') == "Secrétaire Général" ):?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretaire_General">
                                    <i class="fas fa-tachometer-alt"></i>Voir les plaintes
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/Secretaire_General/categorie">
                                    <i class="fas fa-tachometer-alt"></i>Catégorie plainte
                                </a>
                            </li>
                        <?php else:?>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/President">
                                    <i class="fas fa-tachometer-alt"></i>Accueil
                                </a>
                            </li>
                            <li class="active">
                                <a class="js-arrow" href="<?php echo base_url();?>Admin/President/listePlainte">
                                    <i class="fas fa-tachometer-alt"></i>Voir plaintes
                                </a>
                            </li>
                        <?php endif;?>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END HEADER DESKTOP-->