<?php


class Utilisateurs_model extends CI_Model
{
    public function getAllUser()
    {
        $this->db->select('id_user, login, email, type_user, date_ajout');
        $this->db->from('users');
        $this->db->where('date_delete is null');
        $query = $this->db->get();
        return $query->result();
    }
    public function addUser($data)
    {
        $this->db->insert('users', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function suppressionUtilisateur($id, $data)
    {
        $this->db->where('id_user', $id );
        $this->db->update('users', $data);
        return true;
    }
    public function getLastFiveUser(){
        $this->db->select('id_user, login, email, type_user, date_ajout');
        $this->db->from('users');
        $this->db->where('date_delete is null');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }
    public function getLastFiveHistoryUsers(){
        $this->db->select('id_his, login, date_his, action_his');
        $this->db->from(' historique');
        $this->db->join('users as us', 'historique.id_user = us.id_user');
        $this->db->order_by('id_his desc');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }
}