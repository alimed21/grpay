<?php


class Home_model extends CI_Model
{
    public function getInfoUser($id)
    {
        $this->db->select('login, email, p.image_user');
        $this->db->from('users');
        $this->db->join('profile as p', 'users.id_user = p.id_user');
        $this->db->where('users.id_user', $id);
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllEleves($month){
     /*$this->db->select('id_eleve, nom_eleve, prenom_eleve, adresse_eleve, nationalite_eleve, date_naissance, telephone, ancien_club, catégorie, login, date_add, nom_prenom_pere, adresse_pere, tele_pere, fix_pere, mail_pere, nom_prenom_mere, adresse_mere, tele_mere, mail_mere, autorisation, image_eleve, date_paie');
        $this->db->from('eleves');
        $this->db->join('users as us', 'eleves.id_user_add = us.id_user');
        $this->db->where('eleves.date_delete IS NULL');
        $this->db->order_by('id_eleve desc');
        $query = $this->db->get();
        return $query->result();
        $sql = "SELECT e.*, CASE when p.date_paie >= $datePaie THEN p.date_paie  ELSE 'null' END as date_paiement FROM `eleves` e LEFT JOIN paiement p ON e.id_eleve=p.id_eleve";
        $query = $this->db->query($sql);
        return $query->result();*/
        $this->db->select('id_eleve, nom_eleve, prenom_eleve, adresse_eleve, nationalite_eleve, date_naissance, date_paie, telephone, ancien_club, catégorie, date_add, nom_prenom_pere, adresse_pere, tele_pere, fix_pere, mail_pere, nom_prenom_mere, adresse_mere, tele_mere, mail_mere, autorisation, image_eleve');
        $this->db->from("eleves");
        $this->db->where("DATEDIFF(now(),date_paie) > 30");
        $this->db->where('eleves.date_delete IS NULL');
        $this->db->order_by('id_eleve desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllPret(){
        $this->db->select('id_pret, e.nom_encadreur, e.prenom_encadreur, montant_pret, date_pret, login');
        $this->db->from('pret');
        $this->db->join('encadreur as e', 'pret.id_encadreur = e.id_encadreur');
        $this->db->join('users as us', 'pret.user_add_paie = us.id_user');
        $this->db->where('pret_etat IS NULL');
        $this->db->where('date_delete_paie IS NULL');
        $this->db->order_by('pret.id_encadreur desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function validPret($id_pret, $data){
        $this->db->where('id_pret', $id_pret);
        $this->db->update('pret', $data);
        return true;
    }
    public function getAllEleve(){
        $this->db->select('COUNT(id_eleve) as nbre');
        $this->db->from('eleves');
        $this->db->where('date_delete is null');
        $query = $this->db->get();
        return $query->row();
    }
    public function getAllMontant(){
        $this->db->select('SUM(montant_paie) as montant');
        $this->db->from('paiement');
        $this->db->where('date_delete_paie is null');
        $query = $this->db->get();
        return $query->row();
    }
    public function getAllEncadreur(){
        $this->db->select('COUNT(id_encadreur) as encadreur');
        $this->db->from('encadreur');
        $this->db->where('date_delete is null');
        $query = $this->db->get();
        return $query->row();
    }
    public function getAllDepenses(){
        $pret = 1;
        $this->db->select('SUM(montant_pret) as depense');
        $this->db->from('pret');
        $this->db->where('pret_etat', $pret);
        $this->db->where('date_delete_paie is null');
        $query = $this->db->get();
        return $query->row();
    }
    public function getAllDepensesSalaire(){
        $this->db->select('SUM(montant_paie_encad) as depenseSalaire');
        $this->db->from('paie_encadreur');
        $this->db->where('date_delete_paie_encad is null');
        $query = $this->db->get();
        return $query->row();
    }
    public function getAllDepensesAutre(){
        $this->db->select('SUM(montant_dep) as depenseAutre');
        $this->db->from('depense');
        $this->db->where('date_delete is null');
        $query = $this->db->get();
        return $query->row();
    }
    public function getLastPret(){
        $this->db->select('id_pret, e.nom_encadreur, e.prenom_encadreur, montant_pret, date_pret');
        $this->db->from('pret');
        $this->db->join('encadreur as e', 'pret.id_encadreur = e.id_encadreur');
        $this->db->join('users as us', 'pret.user_add_paie = us.id_user');
        $this->db->where('pret_etat IS NULL');
        $this->db->where('date_delete_paie IS NULL');
        $this->db->order_by('pret.id_encadreur desc');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }
    public function getLastRecettes(){
        $this->db->select('id_paie, montant_paie, date_paie');
        $this->db->from('paiement');
        $this->db->where('date_delete_paie is null');
        $this->db->limit(5);
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllHistorique(){
        $this->db->select('id_his, login, date_his, action_his');
        $this->db->from(' historique');
        $this->db->join('users as us', 'historique.id_user = us.id_user');
        $this->db->order_by('id_his desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllPaiement(){
        $this->db->select("id_paie, e.id_eleve, montant_paie, p.date_paie, nom_eleve, prenom_eleve");
        $this->db->from("paiement as p");
        $this->db->join('eleves as e', 'p.id_eleve = e.id_eleve');
        $this->db->where("date_delete_paie IS NULL");
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllPaiementEncadreur(){
        $this->db->select('id_paie_encad, montant_paie_encad, date_add_paie_encad, login, e.nom_encadreur, e.prenom_encadreur');
        $this->db->from('paie_encadreur');
        $this->db->join('encadreur as e', 'paie_encadreur.id_encadreur = e.id_encadreur');
        $this->db->join('users as us', 'paie_encadreur.user_add_paie_encad = us.id_user');
        $this->db->where('user_delete_paie_encad IS NULL');
        $this->db->order_by('paie_encadreur.id_paie_encad desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllDepense(){
        $this->db->select('id_dep, motif_dep, montant_dep, date_dep, d.date_add, us.login');
        $this->db->from('depense as d');
        $this->db->join('users as us', 'd.id_user_add = us.id_user');
        $this->db->where('d.date_delete IS NULL');
        $this->db->order_by('id_dep desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllPretEncadreur(){
        $pret = 1;
        $this->db->select('id_pret,e.nom_encadreur, e.prenom_encadreur, montant_pret, date_pret, pret_etat, pret_rembourser');
        $this->db->from('pret');
        $this->db->join('encadreur as e', 'pret.id_encadreur = e.id_encadreur');
        $this->db->where('date_delete_paie IS NULL');
        $this->db->where('pret_rembourser', $pret);
        $this->db->order_by('date_pret');
        $query = $this->db->get();
        return $query->result();
    }
}