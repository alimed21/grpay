<?php


class Inscription_model extends CI_Model
{
    public function addEnfant($data)
    {
        $this->db->insert('eleves', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getAllEleves(){
        $this->db->select('id_eleve, nom_eleve, prenom_eleve, adresse_eleve, nationalite_eleve, date_naissance, date_paie, telephone, ancien_club, catégorie, login, date_add, nom_prenom_pere, adresse_pere, tele_pere, fix_pere, mail_pere, nom_prenom_mere, adresse_mere, tele_mere, mail_mere, autorisation, image_eleve');
        $this->db->from('eleves');
        $this->db->join('users as us', 'eleves.id_user_add = us.id_user');
        $this->db->where('eleves.date_delete IS NULL');
        $this->db->order_by('id_eleve desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getDetailEleve($id_eleve){
        $this->db->select('id_eleve, nom_eleve, prenom_eleve, adresse_eleve, nationalite_eleve, date_naissance, telephone, ancien_club, catégorie, nom_prenom_pere, adresse_pere, tele_pere, fix_pere, mail_pere, nom_prenom_mere, adresse_mere, tele_mere, mail_mere, autorisation');
        $this->db->from('eleves');
        $this->db->where('id_eleve', $id_eleve);
        $this->db->where('date_delete IS NULL');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllEleveDetail($id_eleve){
        $this->db->select('id_eleve, nom_eleve, prenom_eleve, adresse_eleve, nationalite_eleve, date_naissance, telephone, ancien_club, catégorie, login, date_add, nom_prenom_pere, adresse_pere, tele_pere, fix_pere, mail_pere, nom_prenom_mere, adresse_mere, tele_mere, mail_mere, autorisation, image_eleve');
        $this->db->from('eleves');
        $this->db->join('users as us', 'eleves.id_user_add = us.id_user');
        $this->db->where('eleves.id_eleve', $id_eleve);
        $this->db->where('eleves.date_delete IS NULL');
        $this->db->order_by('id_eleve desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function updateEnfant($data, $id){
        $this->db->where('id_eleve', $id);
        $this->db->update('eleves', $data);
        return true;
    }
    public function supprimer($id_eleve, $data){
        $this->db->where('id_eleve', $id_eleve);
        $this->db->update('eleves', $data);
        return true;
    }
    public function getAllElevesPaie(){
        $this->db->select('id_eleve, nom_eleve, prenom_eleve, adresse_eleve, nationalite_eleve, date_naissance');
        $this->db->from('eleves');
        $this->db->where('date_paie IS NULL');
        $this->db->where('date_delete IS NULL');
        $this->db->order_by('id_eleve desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function addEncadreur($data)
    {
        $this->db->insert('encadreur', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getAllEncadreurs(){
        $this->db->select('id_encadreur, nom_encadreur, prenom_encadreur, nationalite_encadreur	, fonction, piecesjoint, login, date_add');
        $this->db->from('encadreur');
        $this->db->join('users as us', 'encadreur.id_user_add = us.id_user');
        $this->db->where('encadreur.date_delete IS NULL');
        $this->db->order_by('id_encadreur desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function supprimerEncadreur($id_encadreur, $data){
        $this->db->where('id_encadreur', $id_encadreur);
        $this->db->update('encadreur', $data);
        return true;
    }
    public function addPresence($data){
         $this->db->insert('presence', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function addPaiement($data){
        $this->db->insert('paiement', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function updatePaiement($data, $id_eleve){
        $this->db->where('id_eleve', $id_eleve);
        $this->db->update('eleves', $data);
        return true;
    }
    public function addPret($data){
        $this->db->insert('pret', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function search($search)
    {
        $this->db->select("id_paie, e.id_eleve, montant_paie, p.date_paie, nom_eleve, prenom_eleve");
        $this->db->from("paiement as p");
        $this->db->join('eleves as e', 'p.id_eleve = e.id_eleve');
        $this->db->where('p.id_eleve', $search);
        $this->db->where("date_delete_paie IS NULL");
        $query = $this->db->get();
        return $query->result();
    }

    public function getAllElevesNonPayer($month){
        $this->db->select('id_eleve, nom_eleve, prenom_eleve, adresse_eleve, nationalite_eleve, date_naissance, date_paie, telephone, ancien_club, catégorie, date_add, nom_prenom_pere, adresse_pere, tele_pere, fix_pere, mail_pere, nom_prenom_mere, adresse_mere, tele_mere, mail_mere, autorisation, image_eleve');
        $this->db->from("eleves");
        $this->db->where("DATEDIFF(now(),date_paie) > 30");
        $this->db->where('eleves.date_delete IS NULL');
        $this->db->order_by('id_eleve desc');
        $query = $this->db->get();
        return $query->result();
     }
     public function initialiserDatePaie($data){
         $this->db->update('eleves', $data);
         return true;
     }
}