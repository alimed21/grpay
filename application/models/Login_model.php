<?php
/**
 *
 */
class Login_model extends CI_Model
{
    public function can_login($username, $password)
    {
        $this->db->select('id_user, login, email, type_user');
        $this->db->from('users');
        $this->db->where('login', $username);
        $this->db->where('pass', $password);
        $this->db->where('date_delete is null');
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    public function can_loginAdmin($username, $password)
    {
        $this->db->select('id_admin, login_admin');
        $this->db->from('administrateur');
        $this->db->where('login_admin', $username);
        $this->db->where('password_admin', $password);
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function findEmail($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('date_delete is null');
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function findEmailAdmin($email)
    {
        $this->db->select('*');
        $this->db->from('administrateur');
        $this->db->where('email_admin', $email);
        $this->db->where('date_delete is null');
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function resetPassword($data, $email)
    {
        $this->db->where('email', $email);
        $this->db->update('users', $data);
        return true;
    }

    public function resetPasswordAdmin($data, $email)
    {
        $this->db->where('email_admin', $email);
        $this->db->update('administrateur', $data);
        return true;
    }

    public function updatePassword($data, $token)
    {
        $this->db->where('pass', $token);
        $this->db->update('users', $data);
        return true;
    }

    public function updatePasswordAdmin($data, $token)
    {
        $this->db->where('password_admin', $token);
        $this->db->update('administrateur', $data);
        return true;
    }

    public function log_manager($data)
    {
        $this->db->insert('historique', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
}


?>
