<?php


class Encadreur_model extends CI_Model
{
    public function addEncadreur($data)
    {
        $this->db->insert('encadreur', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getAllEncadreurs(){
        $this->db->select('id_encadreur, nom_encadreur, prenom_encadreur, nationalite_encadreur	, fonction, piecesjoint, login, date_add');
        $this->db->from('encadreur');
        $this->db->join('users as us', 'encadreur.id_user_add = us.id_user');
        $this->db->where('encadreur.date_delete IS NULL');
        $this->db->order_by('id_encadreur desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function supprimerEncadreur($id_encadreur, $data){
        $this->db->where('id_encadreur', $id_encadreur);
        $this->db->update('encadreur', $data);
        return true;
    }
    public function addPresence($data){
        $this->db->insert('presence', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function addPret($data){
        $this->db->insert('pret', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getAllPret(){
        $this->db->select('id_pret,e.nom_encadreur, e.prenom_encadreur, montant_pret, date_pret, pret_etat');
        $this->db->from('pret');
        $this->db->join('encadreur as e', 'pret.id_encadreur = e.id_encadreur');
        $this->db->where('date_delete_paie IS NULL');
        $this->db->where('pret_rembourser IS NULL');
        $this->db->order_by('date_pret');
        $query = $this->db->get();
        return $query->result();
    }
    public function addPaieEncadreur($data){
        $this->db->insert('paie_encadreur', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getAllPaieEncadreurs(){
        $this->db->select('id_paie_encad, montant_paie_encad, date_add_paie_encad, login, e.nom_encadreur, e.prenom_encadreur');
        $this->db->from('paie_encadreur');
        $this->db->join('encadreur as e', 'paie_encadreur.id_encadreur = e.id_encadreur');
        $this->db->join('users as us', 'paie_encadreur.user_add_paie_encad = us.id_user');
        $this->db->where('user_delete_paie_encad IS NULL');
        $this->db->order_by('paie_encadreur.id_paie_encad desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function getAllPresences(){
        $this->db->select('id_presence, nom_encadreur, prenom_encadreur, statut, p.date_add');
        $this->db->from('presence as p');
        $this->db->join('encadreur as e', 'p.id_encadreur = e.id_encadreur');
        $this->db->where('p.date_delete IS NULL');
        $this->db->order_by('id_presence desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function addDepense($data){
        $this->db->insert('depense', $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getAllDepenses(){
        $this->db->select('id_dep, motif_dep, montant_dep, date_dep, d.date_add, us.login');
        $this->db->from('depense as d');
        $this->db->join('users as us', 'd.id_user_add = us.id_user');
        $this->db->where('d.date_delete IS NULL');
        $this->db->order_by('id_dep desc');
        $query = $this->db->get();
        return $query->result();
    }
    public function pretRembrouser($id, $data){
        $this->db->where('id_pret', $id);
        $this->db->update('pret', $data);
    }
}