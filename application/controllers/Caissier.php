<?php


class Caissier extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Login_model');
        $this->load->model('Inscription_model');
        $this->load->model('Encadreur_model');
        $this->load->library('form_validation');
        require_once ('./vendor/autoload.php');
        if (!$this->session->userdata('id_user')) {
            redirect('Login');
        }
        if($this->session->userdata('type_user') != "caisser")
        {
            redirect('Login/logout');
        }
    }
    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d');
    }

    public function index(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/ajoutInscription_view');
        $this->load->view('admin/templates/footer');
    }

    public function inscription(){
        $this->form_validation->set_rules('nom', "nom", 'trim|required');
        $this->form_validation->set_rules('prenom', 'prénom ', 'trim|required');
        $this->form_validation->set_rules('adresse', 'adresse', 'trim|required');
        $this->form_validation->set_rules('nationalite', 'nationalité ', 'trim|required');
        $this->form_validation->set_rules('date_naissance', 'date de naissance', 'trim|required');
        $this->form_validation->set_rules('telephone', 'téléphone portable', 'trim|required');
        $this->form_validation->set_rules('type', 'catégorie', 'trim|required');

        $this->form_validation->set_rules('nom_prenom', "nom et prénom", 'trim|required');
        $this->form_validation->set_rules('adresse_pere', 'Adresse ', 'trim|required');
        $this->form_validation->set_rules('phone_pere', 'Téléphone portable', 'trim|required');
        $this->form_validation->set_rules('phoneFix_pere', 'Téléphone domicile', 'trim|required');
        $this->form_validation->set_rules('email_pere', 'Mail ', 'trim|required|valid_email');


        $this->form_validation->set_rules('nom_prenom_mere', "nom et prénom", 'trim|required');
        $this->form_validation->set_rules('adresse_mere', 'Adresse', 'trim|required');
        $this->form_validation->set_rules('phone_mere', 'Téléphone portable', 'trim|required');
        $this->form_validation->set_rules('autorisation', 'autorisation', 'trim|required');

         //Config image
         $config['upload_path'] = './uploads/inscription';
         $config['allowed_types'] = 'jpg|png|jpeg';
         $config['max_size'] = 1999;
         $config['max_width'] = 1500;
         $config['max_height'] = 1500;
         $config['encrypt_name'] = TRUE;
         $this->load->library('upload', $config);
         $this->upload->initialize($config);

        if ($this->form_validation->run() == true)
        {
            //True
            //True
            if (!$this->upload->do_upload('userfile')) {
                $id = $this->session->userdata('id_user');

                $infoUser = $this->Home_model->getInfoUser($id);
                $data['infoUser'] = $infoUser;

                $data['error_message'] = $this->upload->display_errors();
        
                $this->load->view('admin/templates/header', $data);
                $this->load->view('admin/caisse/ajoutInscription_view', $data);
                $this->load->view('admin/templates/footer');
            }
            else{
                //donner enfant
                $nom = $this->input->post('nom');
                $prenom = $this->input->post('prenom');
                $adresse  = $this->input->post('adresse');
                $nationalite    = $this->input->post('nationalite');
                $date_naissance     = $this->input->post('date_naissance');
                $telephone     = $this->input->post('telephone');
                $ancien_club     = $this->input->post('ancien_club');
                $type     = $this->input->post('type');
                $full_path = "uploads/inscription/" . $this->upload->data('file_name');

                //donner pere
                $nom_prenom = $this->input->post('nom_prenom');
                $adresse_pere = $this->input->post('adresse_pere');
                $phone_pere  = $this->input->post('phone_pere');
                $phoneFix_pere    = $this->input->post('phoneFix_pere');
                $email     = $this->input->post('email_pere');

                //donner mere
                $nom_prenom_mere = $this->input->post('nom_prenom_mere');
                $adresse_mere = $this->input->post('adresse_mere');
                $phone_mere  = $this->input->post('phone_mere');
                $autorisation  = $this->input->post('autorisation');

                $id_user_add = $this->session->userdata('id_user');
                $date_add = $this->getDatetimeNow();

                //Rassemnbler ses données dans un tableau
                $data = array(
                    'nom_eleve' => $nom,
                    'prenom_eleve' => $prenom,
                    'adresse_eleve '   => $adresse,
                    'nationalite_eleve'  => $nationalite,
                    'date_naissance' => $date_naissance,
                    'telephone' => $telephone,
                    'image_eleve' => strtolower($full_path),
                    'ancien_club' => $ancien_club,
                    'catégorie '   => $type,
                    'nom_prenom_pere'  => $nom_prenom,
                    'adresse_pere' => $adresse_pere,
                    'tele_pere' => $phone_pere,
                    'fix_pere' => $phoneFix_pere,
                    'mail_pere '   => $email,
                    'nom_prenom_mere'  => $nom_prenom_mere,
                    'adresse_mere' => $adresse_mere,
                    'tele_mere' => $phone_mere,
                    'autorisation' => $autorisation,
                    'id_user_add '   => $id_user_add,
                    'date_add'  => $date_add
                );
                //var_dump($data);die;
                //On va inserée ses données dans la BD
                $addInscription= $this->Inscription_model->addEnfant($data);

                if ($addInscription = true)
                {
                    $action = "Inscription d'un enfant réussi";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Inscription réussi');
                    redirect('Caissier/listesEnfants');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Caissier');
                }

            }
        }
        else
        {
            //False
            $this->index();
        }
    }

    public function listesEnfants(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $eleves = $this->Inscription_model->getAllEleves();
        $data['eleves'] = $eleves;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listeInscription_view');
        $this->load->view('admin/templates/footer');
    }


    public function listesNonPayer(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $month = date('Y-m-d');

        $elevesNonPayer = $this->Inscription_model->getAllElevesNonPayer($month);
        $data['elevesNonPayer'] = $elevesNonPayer;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listeNonPayer', $data);
        $this->load->view('admin/templates/footer');
    }

    /** Fonction convertir une liste en PDF */
    public function pdf($id_eleve)
    {
        //Path to download the pdf file
        $form = $this->Inscription_model->getAllEleveDetail($id_eleve);
        $data['form'] = $form;
        foreach($form as $d)
        {
            $nom = $d->nom_eleve;
            $prenom = $d->prenom_eleve;
        }
        $this->load->view('admin/caisse/listeElevePDF', $data, true);
        $trait = "_";
        $date =  preg_replace('/\s+/', '', date('d-m-Y'));
        $nom_eleve =  preg_replace('/\s+/', '', $nom);
        $prenom_eleve = preg_replace('/\s+/', '', $prenom);
        $name_file = $nom_eleve.$trait.$prenom_eleve.$trait.$date.'.pdf';
        $html =  $this->load->view('admin/caisse/listeElevePDF', $data, true);
        $mpdf = new \Mpdf\Mpdf(['format' => 'A4-P', 'tempDir' => './uploads/pdf/tmp']);
        $mpdf->SetHTMLFooter('<table width="100%" style="font-size: 8pt; color: #000000;">
        <tfoot>
        <tr>
            <td style="width: 70%;border: none;background-color: white;font-style: italic;">Ce document a été généré le {DATE j/m/Y H:i:s}</td>
            <td style="width: 20%;border: none;background-color: white;font-style: italic;" align="center">{PAGENO}/{nbpg}</td>
          </tr>
        </tfoot>

        </table>');
        $mpdf->WriteHTML($html);
        $mpdf->Output($name_file, 'I');

    }

    public function inscriptionModifier($id_eleve){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $elevesDetail = $this->Inscription_model->getDetailEleve($id_eleve);
        $data['elevesDetail'] = $elevesDetail;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/modificationInscription_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function modificationInscription(){
        $this->form_validation->set_rules('nom', "nom", 'trim|required');
        $this->form_validation->set_rules('prenom', 'prénom ', 'trim|required');
        $this->form_validation->set_rules('adresse', 'adresse', 'trim|required');
        $this->form_validation->set_rules('nationalite', 'nationalité ', 'trim|required');
        $this->form_validation->set_rules('date_naissance', 'date de naissance', 'trim|required');
        $this->form_validation->set_rules('telephone', 'téléphone portable', 'trim|required');
        $this->form_validation->set_rules('type', 'catégorie', 'trim|required');

        $this->form_validation->set_rules('nom_prenom', "nom et prénom", 'trim|required');
        $this->form_validation->set_rules('adresse_pere', 'Adresse ', 'trim|required');
        $this->form_validation->set_rules('phone_pere', 'Téléphone portable', 'trim|required');
        $this->form_validation->set_rules('phoneFix_pere', 'Téléphone domicile', 'trim|required');
        $this->form_validation->set_rules('email_pere', 'Mail ', 'trim|required|valid_email');


        $this->form_validation->set_rules('nom_prenom_mere', "nom et prénom", 'trim|required');
        $this->form_validation->set_rules('adresse_mere', 'Adresse', 'trim|required');
        $this->form_validation->set_rules('phone_mere', 'Téléphone portable', 'trim|required');
        $this->form_validation->set_rules('autorisation', 'autorisation', 'trim|required');


        //Config image
        $config['upload_path'] = './uploads/inscription';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 1999;
        $config['max_width'] = 1500;
        $config['max_height'] = 1500;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $id = $this->input->post("id_eleve");

        if ($this->form_validation->run() == true)
        {
            //True
            if (!$this->upload->do_upload('userfile')) {
                //donner enfant
                $nom = $this->input->post('nom');
                $prenom = $this->input->post('prenom');
                $adresse  = $this->input->post('adresse');
                $nationalite    = $this->input->post('nationalite');
                $date_naissance     = $this->input->post('date_naissance');
                $telephone     = $this->input->post('telephone');
                $ancien_club     = $this->input->post('ancien_club');
                $type     = $this->input->post('type');

                //donner pere
                $nom_prenom = $this->input->post('nom_prenom');
                $adresse_pere = $this->input->post('adresse_pere');
                $phone_pere  = $this->input->post('phone_pere');
                $phoneFix_pere    = $this->input->post('phoneFix_pere');
                $email     = $this->input->post('email_pere');

                //donner mere
                $nom_prenom_mere = $this->input->post('nom_prenom_mere');
                $adresse_mere = $this->input->post('adresse_mere');
                $phone_mere  = $this->input->post('phone_mere');
                $autorisation  = $this->input->post('autorisation');

                $id_user_add = $this->session->userdata('id_user');
                $date_add = $this->getDatetimeNow();

                //Rassemnbler ses données dans un tableau
                $data = array(
                    'nom_eleve' => $nom,
                    'prenom_eleve' => $prenom,
                    'adresse_eleve '   => $adresse,
                    'nationalite_eleve'  => $nationalite,
                    'date_naissance' => $date_naissance,
                    'telephone' => $telephone,
                    'ancien_club' => $ancien_club,
                    'catégorie '   => $type,
                    'nom_prenom_pere'  => $nom_prenom,
                    'adresse_pere' => $adresse_pere,
                    'tele_pere' => $phone_pere,
                    'fix_pere' => $phoneFix_pere,
                    'mail_pere '   => $email,
                    'nom_prenom_mere'  => $nom_prenom_mere,
                    'adresse_mere' => $adresse_mere,
                    'tele_mere' => $phone_mere,
                    'autorisation' => $autorisation,
                    'id_user_add '   => $id_user_add,
                    'date_add'  => $date_add
                );

                //On va inserée ses données dans la BD
                $updateInscription= $this->Inscription_model->updateEnfant($data, $id);

                if ($updateInscription = true)
                {
                    $action = "Modification d'un enfant réussi";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Modification réussi');
                    redirect('Caissier/listesEnfants');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Caissier');
                }

            }
            else{
                //donner enfant
                $nom = $this->input->post('nom');
                $prenom = $this->input->post('prenom');
                $adresse  = $this->input->post('adresse');
                $nationalite    = $this->input->post('nationalite');
                $date_naissance     = $this->input->post('date_naissance');
                $telephone     = $this->input->post('telephone');
                $ancien_club     = $this->input->post('ancien_club');
                $type     = $this->input->post('type');
                $full_path = "uploads/inscription/" . $this->upload->data('file_name');

                //donner pere
                $nom_prenom = $this->input->post('nom_prenom');
                $adresse_pere = $this->input->post('adresse_pere');
                $phone_pere  = $this->input->post('phone_pere');
                $phoneFix_pere    = $this->input->post('phoneFix_pere');
                $email     = $this->input->post('email_pere');

                //donner mere
                $nom_prenom_mere = $this->input->post('nom_prenom_mere');
                $adresse_mere = $this->input->post('adresse_mere');
                $phone_mere  = $this->input->post('phone_mere');
                $autorisation  = $this->input->post('autorisation');

                $id_user_add = $this->session->userdata('id_user');
                $date_add = $this->getDatetimeNow();

                //Rassemnbler ses données dans un tableau
                $data = array(
                    'nom_eleve' => $nom,
                    'prenom_eleve' => $prenom,
                    'adresse_eleve '   => $adresse,
                    'nationalite_eleve'  => $nationalite,
                    'date_naissance' => $date_naissance,
                    'telephone' => $telephone,
                    'image_eleve' => strtolower($full_path),
                    'ancien_club' => $ancien_club,
                    'catégorie '   => $type,
                    'nom_prenom_pere'  => $nom_prenom,
                    'adresse_pere' => $adresse_pere,
                    'tele_pere' => $phone_pere,
                    'fix_pere' => $phoneFix_pere,
                    'mail_pere '   => $email,
                    'nom_prenom_mere'  => $nom_prenom_mere,
                    'adresse_mere' => $adresse_mere,
                    'tele_mere' => $phone_mere,
                    'autorisation' => $autorisation,
                    'id_user_add '   => $id_user_add,
                    'date_add'  => $date_add
                );

                //On va modifié ses données dans la BD
                $updateInscription= $this->Inscription_model->updateEnfant($data, $id);

                if ($updateInscription = true)
                {
                    $action = "Modification d'un enfant réussi";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Modification réussi');
                    redirect('Caissier/listesEnfants');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Caissier');
                }
            }
        }
        else
        {
            //False
            $this->inscriptionModifier($id);
        }
    }

    public function inscriptionSupprimer($id_eleve){
        $id_user = $this->session->userdata('id_user');
        $date = $this->getDatetimeNow();

        $data = array(
            'id_user_delete' => $id_user,
            'date_delete' => $date
        );

        $deleteInscription = $this->Inscription_model->supprimer($id_eleve, $data);

        if($deleteInscription = TRUE){
            //Historisation
            $action = "Inscription numéro ".$id_eleve." à été supprimer";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression réussi');
            redirect('Caissier/listesEnfants');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Caissier/listesEnfants');
        }
    }

    /** Encadreur gestion */
    public function ajoutEncadreur(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/ajoutEncadreur_view');
        $this->load->view('admin/templates/footer');
    }

    public function encadreur(){
        $this->form_validation->set_rules('nom', "nom", 'trim|required');
        $this->form_validation->set_rules('prenom', 'prénom ', 'trim|required');
        $this->form_validation->set_rules('nationalite', 'nationalité ', 'trim|required');
        $this->form_validation->set_rules('fonction', 'fonction', 'trim|required');


        //Config image
        $config['upload_path'] = './uploads/encadreur';
        $config['allowed_types'] = 'pdf|docx';
        $config['max_size'] = 5000;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->form_validation->run() == true)
        {
            //True
            if (!$this->upload->do_upload('userfile')) {
                //donner enfant
                $nom = $this->input->post('nom');
                $prenom = $this->input->post('prenom');
                $nationalite    = $this->input->post('nationalite');
                $fonction     = $this->input->post('fonction');
                $id_user_add = $this->session->userdata('id_user');
                $date_add = $this->getDatetimeNow();

                //Rassemnbler ses données dans un tableau
                $data = array(
                    'nom_encadreur' => $nom,
                    'prenom_encadreur' => $prenom,
                    'nationalite_encadreur'  => $nationalite,
                    'fonction' => $fonction,
                    'id_user_add '   => $id_user_add,
                    'date_add'  => $date_add
                );
                //On va inserée ses données dans la BD
                $ajoutEncadreur= $this->Encadreur_model->addEncadreur($data);

                if ($ajoutEncadreur = true)
                {
                    $action = "Inscription d'un encadreur réussi";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Inscription réussi');
                    redirect('Caissier/listesEncadreur');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Caissier/ajoutEncadreur');
                }
            }
            else{
                //donner enfant
                $nom = $this->input->post('nom');
                $prenom = $this->input->post('prenom');
                $nationalite    = $this->input->post('nationalite');
                $fonction     = $this->input->post('fonction');
                $full_path = "uploads/encadreur/" . $this->upload->data('file_name');
                $id_user_add = $this->session->userdata('id_user');
                $date_add = $this->getDatetimeNow();

                //Rassemnbler ses données dans un tableau
                $data = array(
                    'nom_encadreur' => $nom,
                    'prenom_encadreur' => $prenom,
                    'nationalite_encadreur'  => $nationalite,
                    'fonction' => $fonction,
                    'piecesjoint' => strtolower($full_path),
                    'id_user_add '   => $id_user_add,
                    'date_add'  => $date_add
                );
                //On va inserée ses données dans la BD
                $ajoutEncadreur= $this->Encadreur_model->addEncadreur($data);

                if ($ajoutEncadreur = true)
                {
                    $action = "Inscription d'un encadreur réussi";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Inscription réussi');
                    redirect('Caissier/listesEncadreur');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Caissier/ajoutEncadreur');
                }
            }
        }
        else
        {
            //False
            $this->index();
        }
    }

    public function listesEncadreur(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $encadreurs = $this->Encadreur_model->getAllEncadreurs();
        $data['encadreurs'] = $encadreurs;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listeEncadreur', $data);
        $this->load->view('admin/templates/footer');
    }

    public function encadreurSupprimer($id_encadreur){
        $id_user = $this->session->userdata('id_user');
        $date = $this->getDatetimeNow();

        $data = array(
            'id_user_delete' => $id_user,
            'date_delete' => $date
        );

        $deleteEncadreur = $this->Encadreur_model->supprimerEncadreur($id_encadreur, $data);

        if($deleteEncadreur = TRUE){
            //Historisation
            $action = "Encadreur numéro ".$id_encadreur." à été supprimer";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression réussi');
            redirect('Caissier/listesEncadreur');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Caissier/listesEncadreur');
        }
    }

    public function presenceEncadreur($id_encadreur){
        $statut = "Présent";
        $id_user = $this->session->userdata('id_user');
        $date = date('Y-m-d H:s:m');

        $data = array(
            'id_encadreur' => $id_encadreur,
            'statut' => $statut,
            'user_add' => $id_user,
            'date_add' => $date
        );
        $addPresence = $this->Encadreur_model->addPresence($data);

        if($addPresence = TRUE){
            $this->session->set_flashdata('sucess', 'Présence  réussi');
            redirect('Caissier/listesEncadreur');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Caissier/listesEncadreur');
        }
    }
    public function absenceEncadreur($id_encadreur){
        $statut = "Absent";
        $id_user = $this->session->userdata('id_user');
        $date = date('Y-m-d H:s:m');

        $data = array(
            'id_encadreur' => $id_encadreur,
            'statut' => $statut,
            'user_add' => $id_user,
            'date_add' => $date
        );
        $addPresence = $this->Encadreur_model->addPresence($data);

        if($addPresence = TRUE){
            $this->session->set_flashdata('sucess', 'Présence  réussi');
            redirect('Caissier/listesEncadreur');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Caissier/listesEncadreur');
        }
    }

    public function lisetePresence(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listePresences= $this->Encadreur_model->getAllPresences();
        $data['listePresences'] = $listePresences;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listePresences', $data);
        $this->load->view('admin/templates/footer');
    }

    public function listePaiementEleve(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listesPaie= $this->Inscription_model->getAllElevesPaie();
        $data['listesPaie'] = $listesPaie;

        $eleves = $this->Inscription_model->getAllEleves();
        $data['eleves'] = $eleves;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listePaie', $data);
        $this->load->view('admin/templates/footer');
    }

    public function Paiement(){
        $this->form_validation->set_rules('paie', "montant à payer", 'trim|required');
        $this->form_validation->set_rules('datePaie', 'date de paiement ', 'trim|required');

        if($this->form_validation->run() == true)
        {
            //True
            $montant = $this->input->post('paie');
            $datePaie = $this->input->post('datePaie');
            $id_eleve = $this->input->post('id_eleve');
            $id_user_add = $this->session->userdata('id_user');
            $date_add = $this->getDatetimeNow();

            $data = array(
                'id_eleve' => $id_eleve,
                'montant_paie' => $montant,
                'date_paie' => $datePaie,
                'user_add_paie' => $id_user_add,
                'date_add_paie' => $date_add
            );

            $addPaiement = $this->Inscription_model->addPaiement($data);

            if($addPaiement = TRUE){
                //Historisation
                $action = "L'eleve numéro ".$id_eleve." à payer";
                $this->histoirque($action);
                $updatePaiement = $this->paiementEleve($datePaie, $id_eleve);

                if($updatePaiement == TRUE){
                    $this->session->set_flashdata('sucess', 'paiement réussi');
                    redirect('Caissier/listesEnfants');
                }
                else{
                    $this->session->set_flashdata('error', 'paiement non effectuer');
                    redirect('Caissier/listePaiementEleve');
                }

            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Caissier/listePaiementEleve');
            }

        }
        else{
            //False
            $this->listesEnfants();
        }
    }

    public function paiementEleve($datePaie, $id_eleve){
        $data = array(
            'date_paie' => $datePaie
        );

        $updatePaiement = $this->Inscription_model->updatePaiement($data, $id_eleve);

        return $updatePaiement;
    }

    public function Pret(){
        $this->form_validation->set_rules('pret', "montant du prête", 'trim|required');
        $this->form_validation->set_rules('datePret', 'date du prêt ', 'trim|required');

        if($this->form_validation->run() == true)
        {
            //True
            $montant = $this->input->post('pret');
            $datePaie = $this->input->post('datePret');
            $id_encadreur = $this->input->post('id_encadreur');
            $id_user_add = $this->session->userdata('id_user');
            $date_add = $this->getDatetimeNow();

            $data = array(
                'id_encadreur' => $id_encadreur,
                'montant_pret' => $montant,
                'date_pret' => $datePaie,
                'user_add_paie' => $id_user_add,
                'date_add_paie' => $date_add
            );

            $addPaiement = $this->Encadreur_model->addPret($data);

            if($addPaiement = TRUE){
                //Historisation
                $action = "L'encadreur numéro ".$id_encadreur." à demander un prêt";
                $this->histoirque($action);
                $this->session->set_flashdata('sucess', 'paiement réussi');
                redirect('Caissier/listesEncadreur');

            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Caissier/listesEncadreur');
            }

        }
        else{
            //False
            $this->listesEncadreur();
        }
    }

    public function listePret(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listePrets = $this->Encadreur_model->getAllPret();
        $data['listePrets'] = $listePrets;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listePrets', $data);
        $this->load->view('admin/templates/footer');
    }

    public function paiementEncadreur()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $encadreurs = $this->Encadreur_model->getAllEncadreurs();
        $data['encadreurs'] = $encadreurs;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/ajoutPaiement', $data);
        $this->load->view('admin/templates/footer');
    }

    public function PaieEncadreur(){
        $this->form_validation->set_rules('encadreur', "encadreur", 'trim|required');
        $this->form_validation->set_rules('paie', 'paiement ', 'trim|required');

        if($this->form_validation->run() == true)
        {
            //True
            $encadreur = $this->input->post('encadreur');
            $paie = $this->input->post('paie');
            $id_user_add = $this->session->userdata('id_user');
            $date_add = $this->getDatetimeNow();

            $data = array(
                'id_encadreur' => $encadreur,
                'montant_paie_encad' => $paie,
                'date_add_paie_encad' => $date_add,
                'user_add_paie_encad' => $id_user_add
            );

            $addPaiement = $this->Encadreur_model->addPaieEncadreur($data);

            if($addPaiement = TRUE){
                //Historisation
                $action = "L'encadreur numéro ".$encadreur." à reçu un paiement";
                $this->histoirque($action);
                $this->session->set_flashdata('sucess', 'paiement réussi');
                redirect('Caissier/listesPaieEncadreur');

            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Caissier/paiementEncadreur');
            }

        }
        else{
            //False
            $this->paiementEncadreur();
        }
    }

    public function listesPaieEncadreur(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $paieEncadreur = $this->Encadreur_model->getAllPaieEncadreurs();
        $data['paieEncadreur'] = $paieEncadreur;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listePaiementEncadreur', $data);
        $this->load->view('admin/templates/footer');
    }

    public function depense(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/ajoutDepense');
        $this->load->view('admin/templates/footer');
    }
    public function depenseForm(){
        $this->form_validation->set_rules('titre', "motif du dépense", 'trim|required');
        $this->form_validation->set_rules('paie', 'montant à payer ', 'trim|required');
        $this->form_validation->set_rules('datePaie', 'date de paiement', 'trim|required');

        if($this->form_validation->run() == true)
        {
            //True
            $titre = $this->input->post('titre');
            $paie = $this->input->post('paie');
            $datePaie = $this->input->post('datePaie');
            $id_user_add = $this->session->userdata('id_user');
            $date_add = $this->getDatetimeNow();

            $data = array(
                'motif_dep' => $titre,
                'montant_dep' => $paie,
                'date_dep' => $datePaie,
                'id_user_add' => $id_user_add,
                'date_add' => $date_add
            );

            $addDepense = $this->Encadreur_model->addDepense($data);

            if($addDepense = TRUE){
                //Historisation
                $action = "ajout dépense";
                $this->histoirque($action);
                $this->session->set_flashdata('sucess', 'ajout dépense réussi');
                redirect('Caissier/listesDepense');

            }
            else{
                $this->session->set_flashdata('error', 'Veuillez réessayer.');
                redirect('Caissier/depense');
            }
        }
        else{
            //False
            $this->depense();
        }
    }

    public function listesDepense(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $depenses = $this->Encadreur_model->getAllDepenses();
        $data['depenses'] = $depenses;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/listeDepense', $data);
        $this->load->view('admin/templates/footer');
    }

    /** Search paiement */
    public function recherchePaiement(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/searchPaiement');
        $this->load->view('admin/templates/footer');
    }

    public function resultatRecherche(){
        $this->form_validation->set_rules('search', "recherche par n°adhérent", 'trim|required');

        if($this->form_validation->run() == true)
        {
            //True
            $search = $this->input->post('search');

            $resarch = $this->Inscription_model->search($search);

            if($resarch ==TRUE){
                $this->resultatRechercheAffiche($resarch);
            }
            else{
                //False
                $this->resultatRechercheAffiche($resarch);
            }
        }
        else{
            //False
            $this->recherchePaiement();
        }
    }

    public function resultatRechercheAffiche($resarch){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $data['resarch'] = $resarch;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/caisse/resultatRecherche', $data);
        $this->load->view('admin/templates/footer');
    }
    public function pretRembrouser($id_pret){
        $pret = 1;
        $data = array(
            'pret_rembourser' => $pret
        );

        $pretRembrouser = $this->Encadreur_model->pretRembrouser($id_pret, $data);

        if ($pretRembrouser = true)
        {
            $action = "Prêt rembrouse";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Prêt rembrouser');
            redirect('Caissier/listePret');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Caissier/listePret');
        }
    }
    public function initialiser(){
        $id = $this->session->userdata('id_user');
        $etat = null;
        $data = array(
            'date_paie' => $etat
        );
        $updateDatePaie = $this->Inscription_model->initialiserDatePaie($data);

        if ($updateDatePaie = true)
        {
            $action = "Le cassier n° ".$id." a initialisé la liste de paiement";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', "Vous venez d'initialiser la liste de paiement");
            redirect('Caissier/listePaiementEleve');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Caissier/listePaiementEleve');
        }
    }
    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_user'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }

}