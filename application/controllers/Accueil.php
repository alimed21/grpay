<?php


class Accueil extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Home_model');
        $this->load->model('Login_model');
        $this->load->model('Utilisateurs_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('id_user')) {
            redirect('Login');
        }
        if($this->session->userdata('type_user') != "admin")
        {
            redirect('Login/logout');
        }
    }

    function getDatetimeNow()
    {
        $tz_object = new DateTimeZone('Africa/Djibouti');
        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y-m-d');
    }

    public function index(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        /** Eleves inscrit */
        $elevesInscrits = $this->Home_model->getAllEleve();
        $data['elevesInscrits'] = $elevesInscrits;

        /**Recette total */
        $recettesTotal = $this->Home_model->getAllMontant();
        $data['recettesTotal'] = $recettesTotal;

        /** Encadreurs */
        $encadreurs = $this->Home_model->getAllEncadreur();
        $data['encadreurs'] = $encadreurs;

        /** Dépenses total */
        $depenses = $this->Home_model->getAllDepenses();
        $data['depenses'] = $depenses;

        /** Dépenses total */
        $depensesSalaire = $this->Home_model->getAllDepensesSalaire();
        $data['depensesSalaire'] = $depensesSalaire;

        /** Dépenses Autre */
        $depensesAutre = $this->Home_model->getAllDepensesAutre();
        $data['depensesAutre'] = $depensesAutre;

        /** 5 dernières prêt demander */
        $derniersPrets = $this->Home_model->getLastPret();
        $data['derniersPrets'] = $derniersPrets;

        /** Recettes*/
        $lastRecettes = $this->Home_model->getLastRecettes();
        $data['lastRecettes'] = $lastRecettes;

        /** Utilisateurs */
        $UsersFiveLast = $this->Utilisateurs_model->getLastFiveUser();
        $data['UsersFiveLast'] = $UsersFiveLast;

        /** Historiques des utilisateurs */
        $usersHistory = $this->Utilisateurs_model->getLastFiveHistoryUsers();
        $data['usersHistory'] = $usersHistory;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/Accueil_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function ajoutUtilisateur()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/ajoutUtilisateur_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function VerifyForm()
    {
        $this->form_validation->set_rules('username', "nom d'utilisateur", 'trim|required');
        $this->form_validation->set_rules('password', 'mot de passe ', 'trim|required');
        $this->form_validation->set_rules('cnfpassword', 'confirmation du mot de passe', 'trim|required');
        $this->form_validation->set_rules('email', 'email ', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('type', 'type', 'trim|required');

        if ($this->form_validation->run() == true)
        {
            //True
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $cnfpass  = md5($this->input->post('cnfpassword'));
            $email    = $this->input->post('email');
            $type     = $this->input->post('type');
            $date_add = $this->getDatetimeNow();

            if($password == $cnfpass)
            {
                $data = array(
                    'login' => $username,
                    'pass' => $password,
                    'email '   => $email,
                    'type_user'  => $type,
                    'date_ajout' => $date_add
                );

                $addUser = $this->Utilisateurs_model->addUser($data);

                if ($addUser = true)
                {
                    $action = "Ajout utilisateur";
                    $this->histoirque($action);
                    $this->session->set_flashdata('sucess', 'Ajout utilisateur réussi');
                    redirect('Accueil/listesUtilisateurs');
                }
                else{
                    $this->session->set_flashdata('error', 'Veuillez réessayer.');
                    redirect('Accueil');
                }

            }
            else{
                $this->session->set_flashdata('error', 'Les deux mots de passes ne sont pas identités.');
                redirect('Admin/Administrateur/ajoutUtilisateur');
            }
        }
        else{
            $this->ajoutUtilisateur();
        }
    }

    public function listesUtilisateurs(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $Users = $this->Utilisateurs_model->getAllUser();
        $data['Users'] = $Users;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listUtilisateurs_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function SupprimerUtilisateur($id)
    {
        $data = array(
            'user_delete' => $this->session->userdata('id_user'),
            'date_delete' => $this->getDatetimeNow()
        );

        $deleteUser = $this->Utilisateurs_model->suppressionUtilisateur($id, $data);

        if ($deleteUser = true)
        {
            $action = "Suppression d'un utilisateur";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Suppression utilisateur réussi');
            redirect('Accueil/listesUtilisateurs');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Accueil/listesUtilisateurs');
        }
    }
    public function listesPaiements(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;


        $month = date('Y-m-d');

        $eleves = $this->Home_model->getAllEleves($month);
        $data['eleves'] = $eleves;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/listePaie', $data);
        $this->load->view('admin/templates/footer');
    }

    public function listePrets(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listePrets = $this->Home_model->getAllPret();
        $data['listePrets'] = $listePrets;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/demandePret', $data);
        $this->load->view('admin/templates/footer');
    }

    public function validerPret($id_pret){
        $valid = 1;
        $data = array(
            'pret_etat' => $valid
        );

        $vaidPret = $this->Home_model->validPret($id_pret, $data);

        if ($vaidPret = true)
        {
            $action = "Acceptation du prêt n°".$id_pret.".";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Accord du prêt réussi');
            redirect('Accueil/listePrets');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Accueil/listePrets');
        }
    }

    public function rejectPret($id_pret){
        $valid = 0;
        $data = array(
            'pret_etat' => $valid
        );

        $vaidPret = $this->Home_model->validPret($id_pret, $data);

        if ($vaidPret = true)
        {
            $action = "Reject du prêt n°".$id_pret.".";
            $this->histoirque($action);
            $this->session->set_flashdata('sucess', 'Reject du prêt réussi');
            redirect('Accueil/listePrets');
        }
        else{
            $this->session->set_flashdata('error', 'Veuillez réessayer.');
            redirect('Accueil/listePrets');
        }
    }

    public function historiques(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listeHistoriques = $this->Home_model->getAllHistorique();
        $data['listeHistoriques'] = $listeHistoriques;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/historique_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function recettes(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listePayaments = $this->Home_model->getAllPaiement();
        $data['listePayaments'] = $listePayaments;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/recettes_view', $data);
        $this->load->view('admin/templates/footer');
    }

    public function paiementEncadreurs(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listePayamentEncadreur = $this->Home_model->getAllPaiementEncadreur();
        $data['listePayamentEncadreur'] = $listePayamentEncadreur;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/paie_view', $data);
        $this->load->view('admin/templates/footer');
    }
    public function autreDepenses(){
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listeAutreDepense = $this->Home_model->getAllDepense();
        $data['listeAutreDepense'] = $listeAutreDepense;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/autreDepenses_view', $data);
        $this->load->view('admin/templates/footer');
    }
    public function pretEncadreurs()
    {
        $id = $this->session->userdata('id_user');

        $infoUser = $this->Home_model->getInfoUser($id);
        $data['infoUser'] = $infoUser;

        $listePretEncadreur = $this->Home_model->getAllPretEncadreur();
        $data['listePretEncadreur'] = $listePretEncadreur;

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/pages/pretEncadreur_view', $data);
        $this->load->view('admin/templates/footer');
    }
    /** Historique */
    public function histoirque($action)
    {
        $data = array(
            'id_user' =>$this->session->userdata('id_user'),
            'action_his' => $action,
            'date_his' =>$this->getDatetimeNow()
        );
        $this->Login_model->log_manager($data);
    }
}